﻿namespace ExcelInteropt_Test
{
    partial class ExcelConditionalControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCellFile1 = new System.Windows.Forms.TextBox();
            this.txtCellFile2 = new System.Windows.Forms.TextBox();
            this.cbOperation = new System.Windows.Forms.ComboBox();
            this.txtCriteria = new System.Windows.Forms.TextBox();
            this.txtResult1to2 = new System.Windows.Forms.TextBox();
            this.txtResult2to1 = new System.Windows.Forms.TextBox();
            this.lblSuccess = new System.Windows.Forms.Label();
            this.lblExcelCell1 = new System.Windows.Forms.Label();
            this.lblExcelCell2 = new System.Windows.Forms.Label();
            this.lblOperation = new System.Windows.Forms.Label();
            this.lblCriteria = new System.Windows.Forms.Label();
            this.lblResult1to2 = new System.Windows.Forms.Label();
            this.lblResult2to1 = new System.Windows.Forms.Label();
            this.lblPercentage = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.lblCell1Value = new System.Windows.Forms.Label();
            this.lblCell2Value = new System.Windows.Forms.Label();
            this.txtSheetFile1 = new System.Windows.Forms.TextBox();
            this.txtSheetFile2 = new System.Windows.Forms.TextBox();
            this.lblSheetFile1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtRuleName = new System.Windows.Forms.TextBox();
            this.lblRuleName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtCellFile1
            // 
            this.txtCellFile1.Location = new System.Drawing.Point(28, 26);
            this.txtCellFile1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCellFile1.Name = "txtCellFile1";
            this.txtCellFile1.Size = new System.Drawing.Size(100, 22);
            this.txtCellFile1.TabIndex = 0;
            this.txtCellFile1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCellFile2
            // 
            this.txtCellFile2.Location = new System.Drawing.Point(135, 25);
            this.txtCellFile2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCellFile2.Name = "txtCellFile2";
            this.txtCellFile2.Size = new System.Drawing.Size(100, 22);
            this.txtCellFile2.TabIndex = 1;
            this.txtCellFile2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cbOperation
            // 
            this.cbOperation.FormattingEnabled = true;
            this.cbOperation.Items.AddRange(new object[] {
            "Within Tolerance",
            "More Than",
            "Less Than"});
            this.cbOperation.Location = new System.Drawing.Point(241, 26);
            this.cbOperation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbOperation.Name = "cbOperation";
            this.cbOperation.Size = new System.Drawing.Size(164, 24);
            this.cbOperation.TabIndex = 2;
            this.cbOperation.SelectedIndexChanged += new System.EventHandler(this.cbOperation_SelectedIndexChanged);
            // 
            // txtCriteria
            // 
            this.txtCriteria.Location = new System.Drawing.Point(412, 26);
            this.txtCriteria.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCriteria.Name = "txtCriteria";
            this.txtCriteria.Size = new System.Drawing.Size(100, 22);
            this.txtCriteria.TabIndex = 3;
            this.txtCriteria.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResult1to2
            // 
            this.txtResult1to2.Location = new System.Drawing.Point(672, 43);
            this.txtResult1to2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtResult1to2.Name = "txtResult1to2";
            this.txtResult1to2.ReadOnly = true;
            this.txtResult1to2.Size = new System.Drawing.Size(119, 22);
            this.txtResult1to2.TabIndex = 4;
            this.txtResult1to2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtResult2to1
            // 
            this.txtResult2to1.Location = new System.Drawing.Point(672, 84);
            this.txtResult2to1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtResult2to1.Name = "txtResult2to1";
            this.txtResult2to1.ReadOnly = true;
            this.txtResult2to1.Size = new System.Drawing.Size(119, 22);
            this.txtResult2to1.TabIndex = 5;
            this.txtResult2to1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSuccess
            // 
            this.lblSuccess.AutoSize = true;
            this.lblSuccess.Location = new System.Drawing.Point(548, 127);
            this.lblSuccess.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSuccess.Name = "lblSuccess";
            this.lblSuccess.Size = new System.Drawing.Size(48, 17);
            this.lblSuccess.TabIndex = 6;
            this.lblSuccess.Text = "Result";
            // 
            // lblExcelCell1
            // 
            this.lblExcelCell1.AutoSize = true;
            this.lblExcelCell1.Location = new System.Drawing.Point(49, 7);
            this.lblExcelCell1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExcelCell1.Name = "lblExcelCell1";
            this.lblExcelCell1.Size = new System.Drawing.Size(80, 17);
            this.lblExcelCell1.TabIndex = 7;
            this.lblExcelCell1.Text = "1st File Cell";
            // 
            // lblExcelCell2
            // 
            this.lblExcelCell2.AutoSize = true;
            this.lblExcelCell2.Location = new System.Drawing.Point(151, 7);
            this.lblExcelCell2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExcelCell2.Name = "lblExcelCell2";
            this.lblExcelCell2.Size = new System.Drawing.Size(85, 17);
            this.lblExcelCell2.TabIndex = 8;
            this.lblExcelCell2.Text = "2nd File Cell";
            // 
            // lblOperation
            // 
            this.lblOperation.AutoSize = true;
            this.lblOperation.Location = new System.Drawing.Point(285, 7);
            this.lblOperation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOperation.Name = "lblOperation";
            this.lblOperation.Size = new System.Drawing.Size(71, 17);
            this.lblOperation.TabIndex = 9;
            this.lblOperation.Text = "Operation";
            // 
            // lblCriteria
            // 
            this.lblCriteria.AutoSize = true;
            this.lblCriteria.Location = new System.Drawing.Point(433, 7);
            this.lblCriteria.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCriteria.Name = "lblCriteria";
            this.lblCriteria.Size = new System.Drawing.Size(53, 17);
            this.lblCriteria.TabIndex = 10;
            this.lblCriteria.Text = "Criteria";
            // 
            // lblResult1to2
            // 
            this.lblResult1to2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblResult1to2.Location = new System.Drawing.Point(548, 34);
            this.lblResult1to2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResult1to2.Name = "lblResult1to2";
            this.lblResult1to2.Size = new System.Drawing.Size(124, 41);
            this.lblResult1to2.TabIndex = 11;
            this.lblResult1to2.Text = "Result First Against Second";
            this.lblResult1to2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblResult2to1
            // 
            this.lblResult2to1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblResult2to1.Location = new System.Drawing.Point(548, 75);
            this.lblResult2to1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResult2to1.Name = "lblResult2to1";
            this.lblResult2to1.Size = new System.Drawing.Size(124, 41);
            this.lblResult2to1.TabIndex = 12;
            this.lblResult2to1.Text = "Result Second Against First";
            this.lblResult2to1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPercentage
            // 
            this.lblPercentage.AutoSize = true;
            this.lblPercentage.Location = new System.Drawing.Point(520, 28);
            this.lblPercentage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPercentage.Name = "lblPercentage";
            this.lblPercentage.Size = new System.Drawing.Size(20, 17);
            this.lblPercentage.TabIndex = 13;
            this.lblPercentage.Text = "%";
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(28, 97);
            this.btnPreview.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(100, 28);
            this.btnPreview.TabIndex = 14;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // lblCell1Value
            // 
            this.lblCell1Value.AutoSize = true;
            this.lblCell1Value.Location = new System.Drawing.Point(181, 97);
            this.lblCell1Value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCell1Value.Name = "lblCell1Value";
            this.lblCell1Value.Size = new System.Drawing.Size(83, 17);
            this.lblCell1Value.TabIndex = 15;
            this.lblCell1Value.Text = "Cell 1 Value";
            // 
            // lblCell2Value
            // 
            this.lblCell2Value.AutoSize = true;
            this.lblCell2Value.Location = new System.Drawing.Point(181, 113);
            this.lblCell2Value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCell2Value.Name = "lblCell2Value";
            this.lblCell2Value.Size = new System.Drawing.Size(83, 17);
            this.lblCell2Value.TabIndex = 16;
            this.lblCell2Value.Text = "Cell 2 Value";
            // 
            // txtSheetFile1
            // 
            this.txtSheetFile1.Location = new System.Drawing.Point(88, 55);
            this.txtSheetFile1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSheetFile1.Name = "txtSheetFile1";
            this.txtSheetFile1.Size = new System.Drawing.Size(40, 22);
            this.txtSheetFile1.TabIndex = 17;
            this.txtSheetFile1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtSheetFile2
            // 
            this.txtSheetFile2.Location = new System.Drawing.Point(195, 54);
            this.txtSheetFile2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSheetFile2.Name = "txtSheetFile2";
            this.txtSheetFile2.Size = new System.Drawing.Size(40, 22);
            this.txtSheetFile2.TabIndex = 18;
            this.txtSheetFile2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSheetFile1
            // 
            this.lblSheetFile1.AutoSize = true;
            this.lblSheetFile1.Location = new System.Drawing.Point(35, 59);
            this.lblSheetFile1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSheetFile1.Name = "lblSheetFile1";
            this.lblSheetFile1.Size = new System.Drawing.Size(45, 17);
            this.lblSheetFile1.TabIndex = 19;
            this.lblSheetFile1.Text = "Sheet";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Sheet";
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Red;
            this.btnDelete.Location = new System.Drawing.Point(860, 7);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(27, 25);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "X";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(808, 40);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(79, 55);
            this.btnLoad.TabIndex = 22;
            this.btnLoad.Text = "Load Rule";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(808, 97);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 55);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save Rule";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtRuleName
            // 
            this.txtRuleName.Location = new System.Drawing.Point(633, 7);
            this.txtRuleName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRuleName.Name = "txtRuleName";
            this.txtRuleName.Size = new System.Drawing.Size(220, 22);
            this.txtRuleName.TabIndex = 24;
            this.txtRuleName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblRuleName
            // 
            this.lblRuleName.AutoSize = true;
            this.lblRuleName.Location = new System.Drawing.Point(548, 11);
            this.lblRuleName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRuleName.Name = "lblRuleName";
            this.lblRuleName.Size = new System.Drawing.Size(78, 17);
            this.lblRuleName.TabIndex = 25;
            this.lblRuleName.Text = "Rule Name";
            // 
            // ExcelConditionalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblRuleName);
            this.Controls.Add(this.txtRuleName);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblSheetFile1);
            this.Controls.Add(this.txtSheetFile2);
            this.Controls.Add(this.txtSheetFile1);
            this.Controls.Add(this.lblCell2Value);
            this.Controls.Add(this.lblCell1Value);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.lblPercentage);
            this.Controls.Add(this.lblResult2to1);
            this.Controls.Add(this.lblResult1to2);
            this.Controls.Add(this.lblCriteria);
            this.Controls.Add(this.lblOperation);
            this.Controls.Add(this.lblExcelCell2);
            this.Controls.Add(this.lblExcelCell1);
            this.Controls.Add(this.lblSuccess);
            this.Controls.Add(this.txtResult2to1);
            this.Controls.Add(this.txtResult1to2);
            this.Controls.Add(this.txtCriteria);
            this.Controls.Add(this.cbOperation);
            this.Controls.Add(this.txtCellFile2);
            this.Controls.Add(this.txtCellFile1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ExcelConditionalControl";
            this.Size = new System.Drawing.Size(900, 160);
            this.Load += new System.EventHandler(this.ExcelConditionalControl_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ExcelConditionalControl_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCellFile1;
        private System.Windows.Forms.TextBox txtCellFile2;
        private System.Windows.Forms.ComboBox cbOperation;
        private System.Windows.Forms.TextBox txtCriteria;
        private System.Windows.Forms.TextBox txtResult1to2;
        private System.Windows.Forms.TextBox txtResult2to1;
        private System.Windows.Forms.Label lblSuccess;
        private System.Windows.Forms.Label lblExcelCell1;
        private System.Windows.Forms.Label lblExcelCell2;
        private System.Windows.Forms.Label lblOperation;
        private System.Windows.Forms.Label lblCriteria;
        private System.Windows.Forms.Label lblResult1to2;
        private System.Windows.Forms.Label lblResult2to1;
        private System.Windows.Forms.Label lblPercentage;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Label lblCell1Value;
        private System.Windows.Forms.Label lblCell2Value;
        private System.Windows.Forms.TextBox txtSheetFile1;
        private System.Windows.Forms.TextBox txtSheetFile2;
        private System.Windows.Forms.Label lblSheetFile1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtRuleName;
        private System.Windows.Forms.Label lblRuleName;
    }
}
