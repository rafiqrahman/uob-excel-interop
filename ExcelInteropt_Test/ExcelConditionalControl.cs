﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelInteropt_Test
{
    public partial class ExcelConditionalControl : UserControl
    {
        public class ExcelConditionalEventArgs : EventArgs
        {
            public string senderName { get; set; }
        }

        public event EventHandler<ExcelConditionalEventArgs> PreviewClickedHandler;
        protected virtual void OnPreviewClicked(object sender)
        {
            this.PreviewClickedHandler?.Invoke(this, new ExcelConditionalEventArgs
            {
                senderName = this.Name
            });
        }

        public event EventHandler<ExcelConditionalEventArgs> DeleteClickedHandler;
        protected virtual void OnDeleteClicked(object sender)
        {
            this.DeleteClickedHandler?.Invoke(this, new ExcelConditionalEventArgs
            {
                senderName = this.Name
            });
        }

        public event EventHandler<ExcelConditionalEventArgs> LoadClickedHandler;
        protected virtual void OnLoadClicked(object sender)
        {
            this.LoadClickedHandler?.Invoke(this, new ExcelConditionalEventArgs
            {
                senderName = this.Name
            });
        }

        public event EventHandler<ExcelConditionalEventArgs> SaveClickedHandler;
        protected virtual void OnSaveClicked(object sender)
        {
            this.SaveClickedHandler?.Invoke(this, new ExcelConditionalEventArgs
            {
                senderName = this.Name
            });
        }

        public string RuleName
        {
            get
            {
                return txtRuleName.Text;
            }
            set
            {
                txtRuleName.Text = value;
            }
        }

        public string Cell1Value
        {
            get
            {
                return txtCellFile1.Text;
            }
            set
            {
                txtCellFile1.Text = value;
            }
        }

        public string Cell2Value
        {
            get
            {
                return txtCellFile2.Text;
            }
            set
            {
                txtCellFile2.Text = value;
            }
        }

        public string Sheet1Value
        {
            get
            {
                return txtSheetFile1.Text;
            }
            set
            {
                txtSheetFile1.Text = value;
            }
        }

        public string Sheet2Value
        {
            get
            {
                return txtSheetFile2.Text;
            }
            set
            {
                txtSheetFile2.Text = value;
            }
        }

        public int Operation
        {
            get
            {
                return cbOperation.SelectedIndex;
            }
            set
            {
                cbOperation.SelectedIndex = value;
            }
        }

        public string CriteriaValue
        {
            get
            {
                return txtCriteria.Text;
            }
            set
            {
                txtCriteria.Text = value;
            }
        }

        public string Result1Agaist2
        {
            get
            {
                return txtResult1to2.Text;
            }
            set
            {
                txtResult1to2.Text = value;
            }
        }

        public string Result2Agaist1
        {
            get
            {
                return txtResult2to1.Text;
            }
            set
            {
                txtResult2to1.Text = value;
            }
        }

        public string FinalResult
        {
            get
            {
                return lblSuccess.Text;
            }
            set
            {
                lblSuccess.Text = value;
            }
        }

        public void SetColorResult1Agaist2(Color color)
        {
            Action action = new Action(() =>
            {
                lblResult1to2.BackColor = color;
            });
            IAsyncResult resultIA = this.BeginInvoke(action);
            this.EndInvoke(resultIA);
        }

        public void SetColorResult2Agaist1(Color color)
        {
            Action action = new Action(() =>
            {
                lblResult2to1.BackColor = color;
            });
            IAsyncResult resultIA = this.BeginInvoke(action);
            this.EndInvoke(resultIA);
        }

        public void SetColorFinalResult(Color color)
        {
            Action action = new Action(() =>
            {
                lblSuccess.BackColor = color;
            });
            IAsyncResult resultIA = this.BeginInvoke(action);
            this.EndInvoke(resultIA);
        }

        public string Cell1PreviewValue
        {
            get
            {
                return lblCell1Value.Text;
            }
            set
            {
                lblCell1Value.Visible = true;
                lblCell1Value.Text = value;
            }
        }

        public string Cell2PreviewValue
        {
            get
            {
                return lblCell2Value.Text;
            }
            set
            {
                lblCell2Value.Visible = true;
                lblCell2Value.Text = value;
            }
        }

        public ExcelConditionalControl()
        {
            InitializeComponent();
        }

        private void cbOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOperation.SelectedIndex == 0)
            {
                lblPercentage.Visible = true;
            }
            else
            {
                lblPercentage.Visible = false;
            }
        }

        private void ExcelConditionalControl_Load(object sender, EventArgs e)
        {
            ResetLabelView();
        }

        public void ResetLabelView()
        {
            Action action = new Action(() =>
            {
                lblCell1Value.Visible = false;
                lblCell1Value.Text = "";
                lblCell2Value.Visible = false;
                lblCell2Value.Text = "";
                lblPercentage.Visible = false;
                lblResult1to2.BackColor = SystemColors.ControlLight;
                lblResult2to1.BackColor = SystemColors.ControlLight;
                lblSuccess.BackColor = SystemColors.ControlLight;
            });
            IAsyncResult resultIA = this.BeginInvoke(action);
            this.EndInvoke(resultIA);
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            OnPreviewClicked(this);
        }

        private void ExcelConditionalControl_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rect = new Rectangle(new Point(0, 0), this.ClientRectangle.Size);
            System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            //Pen pen = new Pen(Brushes.Black, 10);
            Pen pen = new Pen(Color.Blue, 4);
            System.Drawing.Graphics formGraphics;
            formGraphics = this.CreateGraphics();
            rect.Inflate(-2, -2);
            formGraphics.DrawRectangle(pen, rect);
            myBrush.Dispose();
            formGraphics.Dispose();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OnDeleteClicked(this);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            OnSaveClicked(this);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OnLoadClicked(this);
        }
    }
}
