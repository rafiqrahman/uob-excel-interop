﻿namespace ExcelInteropt_Test
{
    partial class ExcelFileLoadControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlExcelWB = new System.Windows.Forms.Panel();
            this.txtWorkbookInfo = new System.Windows.Forms.TextBox();
            this.txtWorkbookFilePath = new System.Windows.Forms.TextBox();
            this.btnLoadWorkbook = new System.Windows.Forms.Button();
            this.btnUnloadWorkbook = new System.Windows.Forms.Button();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.pnlExcelWB.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlExcelWB
            // 
            this.pnlExcelWB.Controls.Add(this.txtTitle);
            this.pnlExcelWB.Controls.Add(this.btnUnloadWorkbook);
            this.pnlExcelWB.Controls.Add(this.txtWorkbookInfo);
            this.pnlExcelWB.Controls.Add(this.txtWorkbookFilePath);
            this.pnlExcelWB.Controls.Add(this.btnLoadWorkbook);
            this.pnlExcelWB.Location = new System.Drawing.Point(3, 3);
            this.pnlExcelWB.Name = "pnlExcelWB";
            this.pnlExcelWB.Size = new System.Drawing.Size(427, 268);
            this.pnlExcelWB.TabIndex = 4;
            // 
            // txtWorkbookInfo
            // 
            this.txtWorkbookInfo.Location = new System.Drawing.Point(4, 55);
            this.txtWorkbookInfo.Multiline = true;
            this.txtWorkbookInfo.Name = "txtWorkbookInfo";
            this.txtWorkbookInfo.ReadOnly = true;
            this.txtWorkbookInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtWorkbookInfo.Size = new System.Drawing.Size(420, 158);
            this.txtWorkbookInfo.TabIndex = 6;
            // 
            // txtWorkbookFilePath
            // 
            this.txtWorkbookFilePath.Location = new System.Drawing.Point(120, 4);
            this.txtWorkbookFilePath.Multiline = true;
            this.txtWorkbookFilePath.Name = "txtWorkbookFilePath";
            this.txtWorkbookFilePath.ReadOnly = true;
            this.txtWorkbookFilePath.Size = new System.Drawing.Size(304, 44);
            this.txtWorkbookFilePath.TabIndex = 5;
            // 
            // btnLoadWorkbook
            // 
            this.btnLoadWorkbook.Location = new System.Drawing.Point(4, 4);
            this.btnLoadWorkbook.Margin = new System.Windows.Forms.Padding(4);
            this.btnLoadWorkbook.Name = "btnLoadWorkbook";
            this.btnLoadWorkbook.Size = new System.Drawing.Size(109, 44);
            this.btnLoadWorkbook.TabIndex = 4;
            this.btnLoadWorkbook.Text = "Load Workbook";
            this.btnLoadWorkbook.UseVisualStyleBackColor = true;
            this.btnLoadWorkbook.Click += new System.EventHandler(this.btnLoadWorkbook_Click);
            // 
            // btnUnloadWorkbook
            // 
            this.btnUnloadWorkbook.Location = new System.Drawing.Point(318, 220);
            this.btnUnloadWorkbook.Margin = new System.Windows.Forms.Padding(4);
            this.btnUnloadWorkbook.Name = "btnUnloadWorkbook";
            this.btnUnloadWorkbook.Size = new System.Drawing.Size(109, 44);
            this.btnUnloadWorkbook.TabIndex = 7;
            this.btnUnloadWorkbook.Text = "Unload Workbook";
            this.btnUnloadWorkbook.UseVisualStyleBackColor = true;
            this.btnUnloadWorkbook.Click += new System.EventHandler(this.btnUnloadWorkbook_Click);
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(3, 231);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ReadOnly = true;
            this.txtTitle.Size = new System.Drawing.Size(303, 22);
            this.txtTitle.TabIndex = 8;
            // 
            // ExcelFileLoadControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlExcelWB);
            this.Name = "ExcelFileLoadControl";
            this.Size = new System.Drawing.Size(434, 274);
            this.pnlExcelWB.ResumeLayout(false);
            this.pnlExcelWB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlExcelWB;
        private System.Windows.Forms.TextBox txtWorkbookInfo;
        private System.Windows.Forms.TextBox txtWorkbookFilePath;
        private System.Windows.Forms.Button btnLoadWorkbook;
        private System.Windows.Forms.Button btnUnloadWorkbook;
        private System.Windows.Forms.TextBox txtTitle;
    }
}
