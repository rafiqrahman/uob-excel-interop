﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExcelInteropt_Test
{
    public partial class ExcelFileLoadControl : UserControl
    {
        public class ExcelWorkbookEventArgs : EventArgs
        {
            public string senderName { get; set; }
        }

        public event EventHandler<ExcelWorkbookEventArgs> LoadWBClickedHandler;
        protected virtual void OnWBLoadClicked(object sender)
        {
            this.LoadWBClickedHandler?.Invoke(this, new ExcelWorkbookEventArgs
            {
                senderName = this.Name
            });
        }

        public event EventHandler<ExcelWorkbookEventArgs> UnloadWBClickedHandler;
        protected virtual void OnWBUnloadClicked(object sender)
        {
            this.UnloadWBClickedHandler?.Invoke(this, new ExcelWorkbookEventArgs
            {
                senderName = this.Name
            });
        }

        public string Title
        {
            get
            {
                return txtTitle.Text;
            }
            set
            {
                txtTitle.Text = value;
            }
        }

        public string WorkbookInfo
        {
            get
            {
                return txtWorkbookInfo.Text;
            }
            set
            {
                txtWorkbookInfo.Text = value;
            }
        }

        public string WorkbookFilePath
        {
            get
            {
                return txtWorkbookFilePath.Text;
            }
            set
            {
                txtWorkbookFilePath.Text = value;
            }
        }

        public bool LoadButtonEnable
        {
            get
            {
                return btnLoadWorkbook.Enabled;
            }
            set
            {
                btnLoadWorkbook.Enabled = value;
            }
        }

        public bool UnloadButtonEnable
        {
            get
            {
                return btnUnloadWorkbook.Enabled;
            }
            set
            {
                btnUnloadWorkbook.Enabled = value;
            }
        }

        public ExcelFileLoadControl()
        {
            InitializeComponent();
            btnUnloadWorkbook.Enabled = false;
        }

        private void btnLoadWorkbook_Click(object sender, EventArgs e)
        {
            OnWBLoadClicked(this);
        }

        private void btnUnloadWorkbook_Click(object sender, EventArgs e)
        {
            OnWBUnloadClicked(this);
        }
    }
}
