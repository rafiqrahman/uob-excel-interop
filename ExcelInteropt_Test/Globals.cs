﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelInteropt_Test
{
    public class ExcelRule
    {
        public string RuleName { get; set; }
        public string Cell1 { get; set; }
        public string Cell2 { get; set; }
        public int Sheet1 { get; set; }
        public int Sheet2 { get; set; }
        public int Operation { get; set; }
        public string Criteria { get; set; }
    }

    public class ExcelEvaluation
    {
        public double Operand1 { get; set; }
        public double Operand2 { get; set; }
        public int Operator { get; set; }
        public double Criteria { get; set; }
    }

    public class BulkExcelRule
    {
        public List<ExcelRule> xlRules { get; set; } = new List<ExcelRule>();
    }
}
