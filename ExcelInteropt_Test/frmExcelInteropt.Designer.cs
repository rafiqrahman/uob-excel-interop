﻿namespace ExcelInteropt_Test
{
    partial class frmExcelInteropt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlExcelControl = new System.Windows.Forms.Panel();
            this.btnAddExcelCondControl = new System.Windows.Forms.Button();
            this.btnEvaluate = new System.Windows.Forms.Button();
            this.excelFileLoadControl2 = new ExcelInteropt_Test.ExcelFileLoadControl();
            this.excelFileLoadControl1 = new ExcelInteropt_Test.ExcelFileLoadControl();
            this.btnLoadBulkRule = new System.Windows.Forms.Button();
            this.btnSaveBulkRule = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pnlExcelControl
            // 
            this.pnlExcelControl.Location = new System.Drawing.Point(10, 238);
            this.pnlExcelControl.Name = "pnlExcelControl";
            this.pnlExcelControl.Size = new System.Drawing.Size(675, 130);
            this.pnlExcelControl.TabIndex = 1;
            // 
            // btnAddExcelCondControl
            // 
            this.btnAddExcelCondControl.Location = new System.Drawing.Point(691, 238);
            this.btnAddExcelCondControl.Name = "btnAddExcelCondControl";
            this.btnAddExcelCondControl.Size = new System.Drawing.Size(111, 50);
            this.btnAddExcelCondControl.TabIndex = 2;
            this.btnAddExcelCondControl.Text = "Add Excel Control";
            this.btnAddExcelCondControl.UseVisualStyleBackColor = true;
            this.btnAddExcelCondControl.Click += new System.EventHandler(this.btnAddExcelCondControl_Click);
            // 
            // btnEvaluate
            // 
            this.btnEvaluate.Location = new System.Drawing.Point(691, 318);
            this.btnEvaluate.Name = "btnEvaluate";
            this.btnEvaluate.Size = new System.Drawing.Size(111, 50);
            this.btnEvaluate.TabIndex = 5;
            this.btnEvaluate.Text = "Evaluate";
            this.btnEvaluate.UseVisualStyleBackColor = true;
            this.btnEvaluate.Click += new System.EventHandler(this.btnEvaluate_Click);
            // 
            // excelFileLoadControl2
            // 
            this.excelFileLoadControl2.LoadButtonEnable = true;
            this.excelFileLoadControl2.Location = new System.Drawing.Point(359, 10);
            this.excelFileLoadControl2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.excelFileLoadControl2.Name = "excelFileLoadControl2";
            this.excelFileLoadControl2.Size = new System.Drawing.Size(326, 223);
            this.excelFileLoadControl2.TabIndex = 4;
            this.excelFileLoadControl2.Title = "";
            this.excelFileLoadControl2.UnloadButtonEnable = false;
            this.excelFileLoadControl2.WorkbookFilePath = "";
            this.excelFileLoadControl2.WorkbookInfo = "";
            // 
            // excelFileLoadControl1
            // 
            this.excelFileLoadControl1.LoadButtonEnable = true;
            this.excelFileLoadControl1.Location = new System.Drawing.Point(9, 10);
            this.excelFileLoadControl1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.excelFileLoadControl1.Name = "excelFileLoadControl1";
            this.excelFileLoadControl1.Size = new System.Drawing.Size(326, 223);
            this.excelFileLoadControl1.TabIndex = 3;
            this.excelFileLoadControl1.Title = "";
            this.excelFileLoadControl1.UnloadButtonEnable = false;
            this.excelFileLoadControl1.WorkbookFilePath = "";
            this.excelFileLoadControl1.WorkbookInfo = "";
            // 
            // btnLoadBulkRule
            // 
            this.btnLoadBulkRule.Location = new System.Drawing.Point(690, 12);
            this.btnLoadBulkRule.Name = "btnLoadBulkRule";
            this.btnLoadBulkRule.Size = new System.Drawing.Size(111, 50);
            this.btnLoadBulkRule.TabIndex = 6;
            this.btnLoadBulkRule.Text = "Load Bulk Rule";
            this.btnLoadBulkRule.UseVisualStyleBackColor = true;
            this.btnLoadBulkRule.Click += new System.EventHandler(this.btnLoadBulkRule_Click);
            // 
            // btnSaveBulkRule
            // 
            this.btnSaveBulkRule.Location = new System.Drawing.Point(690, 90);
            this.btnSaveBulkRule.Name = "btnSaveBulkRule";
            this.btnSaveBulkRule.Size = new System.Drawing.Size(111, 50);
            this.btnSaveBulkRule.TabIndex = 7;
            this.btnSaveBulkRule.Text = "Save Bulk Rule";
            this.btnSaveBulkRule.UseVisualStyleBackColor = true;
            this.btnSaveBulkRule.Click += new System.EventHandler(this.btnSaveBulkRule_Click);
            // 
            // frmExcelInteropt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(838, 388);
            this.Controls.Add(this.btnSaveBulkRule);
            this.Controls.Add(this.btnLoadBulkRule);
            this.Controls.Add(this.btnEvaluate);
            this.Controls.Add(this.excelFileLoadControl2);
            this.Controls.Add(this.excelFileLoadControl1);
            this.Controls.Add(this.btnAddExcelCondControl);
            this.Controls.Add(this.pnlExcelControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.Name = "frmExcelInteropt";
            this.Text = "Excel Interopt Test";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmExcelInteropt_FormClosed);
            this.Load += new System.EventHandler(this.frmExcelInteropt_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlExcelControl;
        private System.Windows.Forms.Button btnAddExcelCondControl;
        private ExcelFileLoadControl excelFileLoadControl1;
        private ExcelFileLoadControl excelFileLoadControl2;
        private System.Windows.Forms.Button btnEvaluate;
        private System.Windows.Forms.Button btnLoadBulkRule;
        private System.Windows.Forms.Button btnSaveBulkRule;
    }
}

