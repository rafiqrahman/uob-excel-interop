﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;

namespace ExcelInteropt_Test
{
    public partial class frmExcelInteropt : Form
    {
        int maxHeight = 0;

        Excel.Workbook[] wbArray = new Excel.Workbook[2];
        Excel.Workbook dummyWB;

        int runningNumber = 0;

        List<ExcelConditionalControl> excelControls = new List<ExcelConditionalControl>();
        List<ExcelFileLoadControl> xlFileLoadControls = new List<ExcelFileLoadControl>();

        Excel.Application appExcel;
        //Excel.Worksheet ws;
        //Excel.Workbook wb;

        public frmExcelInteropt()
        {
            InitializeComponent();
        }

        private void frmExcelInteropt_Load(object sender, EventArgs e)
        {
            appExcel = new Excel.Application();
            excelFileLoadControl1.LoadWBClickedHandler += ExcelFileLoadControl_LoadWBClickedHandler;
            excelFileLoadControl1.UnloadWBClickedHandler += ExcelFileLoadControl_UnloadWBClickedHandler;
            excelFileLoadControl2.LoadWBClickedHandler += ExcelFileLoadControl_LoadWBClickedHandler;
            excelFileLoadControl2.UnloadWBClickedHandler += ExcelFileLoadControl_UnloadWBClickedHandler;

            excelFileLoadControl1.Title = "First Excel Workbook";
            excelFileLoadControl2.Title = "Second Excel Workbook";

            xlFileLoadControls.Add(excelFileLoadControl1);
            xlFileLoadControls.Add(excelFileLoadControl2);

            #region For Testing
            //appExcel = new Excel.Application();
            //appExcel.Visible = true;

            //appExcel.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet);
            //wb = appExcel.ActiveWorkbook;
            //ws = wb.Sheets[1];
            //ws.Name = "Rafiq";

            //ws.Cells[2, 2].value = "Rafiq";
            //ws.Cells[2, 3].Formula = $"=SUM($A$1:$A$5)";

            //ExcelConditionalControl excelCont = new ExcelConditionalControl();
            //excelCont.Name = $"exCont{runningNumber}";
            //excelCont.PreviewClickedHandler += ExcelConditionalControl_PreviewClickedHandler;
            //excelControls.Add(excelCont);
            //pnlExcelControl.Controls.Add(excelControls[runningNumber]);

            //appExcel = new Excel.Application();
            //appExcel.Visible = true;
            //wb = appExcel.Workbooks.Open(@"C:\Users\rafiq\OneDrive\Desktop\ExcelTest.xlsx");
            //ws = wb.Worksheets[1];

            //Excel.Range activeRange = ws.Cells.Range["A1", "C10"];
            //activeRange.AutoFilter(2, "Blue");

            //ws.AutoFilterMode = false;

            //Excel.Range activeRange2 = ws.Cells.Range["A15", "C24"];
            //activeRange2.AutoFilter(2, "Blue");
            #endregion
        }

        private async void ExcelFileLoadControl_UnloadWBClickedHandler(object sender, ExcelFileLoadControl.ExcelWorkbookEventArgs e)
        {
            ExcelFileLoadControl currentControl = (ExcelFileLoadControl)sender;

            int? indexQuestion = await GetControllerIndex(e.senderName);
            int controlIndex = int.Parse((indexQuestion - 1).ToString());

            DialogResult dialogResult = MessageBox.Show("Unload Excel Workbook?", "Unload Workbook Confirmation", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question);

            if (dialogResult == DialogResult.OK)
            {
                if (controlIndex == 1 && dummyWB != null)
                {
                    dummyWB = null;
                    wbArray[controlIndex] = null;
                    currentControl.LoadButtonEnable = true;
                    currentControl.UnloadButtonEnable = false;
                    currentControl.WorkbookFilePath = "";
                    currentControl.WorkbookInfo = "";
                }
                else
                {
                    wbArray[controlIndex].Close(Excel.XlSaveAction.xlDoNotSaveChanges);
                    wbArray[controlIndex] = null;
                    currentControl.LoadButtonEnable = true;
                    currentControl.UnloadButtonEnable = false;
                    currentControl.WorkbookFilePath = "";
                    currentControl.WorkbookInfo = "";
                }

                if (controlIndex == 0 && dummyWB != null)
                {
                    dummyWB = null;
                    wbArray[1] = null;
                    xlFileLoadControls[1].LoadButtonEnable = true;
                    xlFileLoadControls[1].UnloadButtonEnable = false;
                    xlFileLoadControls[1].WorkbookFilePath = "";
                    xlFileLoadControls[1].WorkbookInfo = "";
                }
            }
        }

        private async void ExcelFileLoadControl_LoadWBClickedHandler(object sender, ExcelFileLoadControl.ExcelWorkbookEventArgs e)
        {
            ExcelFileLoadControl currentControl = (ExcelFileLoadControl)sender;

            int? indexQuestion = await GetControllerIndex(e.senderName);
            int controlIndex = int.Parse((indexQuestion - 1).ToString());

            OpenFileDialog fdOpen = new OpenFileDialog();

            fdOpen.DefaultExt = "xlsx;xls";
            fdOpen.Filter = "Excel Files|*.xlsx;*.xls";
            fdOpen.Title = "Open Excel File";

            DialogResult dialogResult = fdOpen.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                wbArray[controlIndex] = appExcel.Workbooks.Open(fdOpen.FileName);

                currentControl.WorkbookFilePath = fdOpen.FileName;

                string textAppend = wbArray[controlIndex].Worksheets.Count == 1 ? "worksheet" : "worksheets";

                currentControl.WorkbookInfo = $"This Excel Workbook has {wbArray[controlIndex].Worksheets.Count} {textAppend}";
                currentControl.WorkbookInfo += "\r\n";

                for (int index = 1; index <= wbArray[controlIndex].Worksheets.Count; index++)
                {
                    currentControl.WorkbookInfo += ($"\r\n{index} : {wbArray[controlIndex].Worksheets[index].Name}");
                }

                currentControl.UnloadButtonEnable = true;
                currentControl.LoadButtonEnable = false;
            }
        }

        private async void ExcelConditionalControl_PreviewClickedHandler(object sender, ExcelConditionalControl.ExcelConditionalEventArgs e)
        {
            ExcelConditionalControl currentControl = (ExcelConditionalControl)sender;

            int? indexQuestion = await GetControllerIndex(e.senderName);
            int controlIndex = int.Parse((indexQuestion - 1).ToString());

            currentControl.Cell1PreviewValue = "";
            currentControl.Cell2PreviewValue = "";

            if (wbArray[0] == null)
            {
                MessageBox.Show("Excel Workbook 1 not loaded", "Workbook Not Loaded Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (wbArray[1] == null)
            {
                DialogResult dialogResult = MessageBox.Show("Excel Workbook 2 not loaded. Use Workbook 1 for Comparision of Cells?", "Workbook Not Loaded Error",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (dialogResult == DialogResult.OK)
                {
                    WorkbookTwoIsOne();
                }
                else
                {
                    return;
                }
            }
            try
            {
                currentControl.Cell1PreviewValue = "Cell 1 Value : " +
                    wbArray[0]
                    .Worksheets[int.Parse(currentControl.Sheet1Value)]
                    .Range[currentControl.Cell1Value]
                    .Value2.ToString();

                currentControl.Cell2PreviewValue = "Cell 2 Value : " +
                    wbArray[1]
                    .Worksheets[int.Parse(currentControl.Sheet2Value)]
                    .Range[currentControl.Cell2Value]
                    .Value2.ToString();

            }
            catch (Exception err)
            {
                MessageBox.Show("Please ensure all fields are typed correctly!", "Some Input Fields Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAddExcelCondControl_Click(object sender, EventArgs e)
        {
            ExcelConditionalControl excelCont = new ExcelConditionalControl();
            excelCont.Name = $"exCont{runningNumber}";
            excelControls.Add(excelCont);
            excelCont.PreviewClickedHandler += ExcelConditionalControl_PreviewClickedHandler;
            excelCont.DeleteClickedHandler += ExcelCont_DeleteClickedHandler;
            excelCont.SaveClickedHandler += ExcelCont_SaveClickedHandler;
            excelCont.LoadClickedHandler += ExcelCont_LoadClickedHandler;
            pnlExcelControl.Controls.Add(excelControls[runningNumber]);
            excelControls[runningNumber].Dock = DockStyle.Bottom;
            pnlExcelControl.Height = excelControls[runningNumber].Height * (runningNumber + 1);
            runningNumber++;


            if (runningNumber == 4)
            {
                maxHeight = this.Height;
            }

            if (runningNumber > 4)
            {
                this.AutoSize = false;
                this.AutoScroll = true;
                this.Height = maxHeight;
            }
        }

        private async void ExcelCont_LoadClickedHandler(object sender, ExcelConditionalControl.ExcelConditionalEventArgs e)
        {
            ExcelConditionalControl currentControl = (ExcelConditionalControl)sender;
            ExcelRule xlRule = new ExcelRule();

            OpenFileDialog fdOpen = new OpenFileDialog();
            fdOpen.Filter = "Excel Rule|*.exr";
            fdOpen.DefaultExt = "exr";
            fdOpen.Title = "Save Excel Rule";

            DialogResult dialogResult = fdOpen.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                FileStream fs = new FileStream(fdOpen.FileName, FileMode.Open);

                byte[] configFromFileBytes = new byte[fs.Length];
                int fileSize = fs.Read(configFromFileBytes, 0, configFromFileBytes.Length);
                string configFromFileString = Encoding.ASCII.GetString(configFromFileBytes);

                try
                {
                    xlRule = JsonConvert.DeserializeObject<ExcelRule>(configFromFileString);
                }
                catch (Exception err)
                {
                    MessageBox.Show("File Read Error. Please choose valid exr file", "File Read Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                finally
                {
                    fs.Close();
                }

                currentControl.Cell1Value = xlRule.Cell1;
                currentControl.Cell2Value = xlRule.Cell2;
                currentControl.Sheet1Value = xlRule.Sheet1.ToString();
                currentControl.Sheet2Value = xlRule.Sheet2.ToString();
                currentControl.RuleName = xlRule.RuleName;
                currentControl.CriteriaValue = xlRule.Criteria;
                currentControl.Operation = xlRule.Operation;
            }
        }

        private async void ExcelCont_SaveClickedHandler(object sender, ExcelConditionalControl.ExcelConditionalEventArgs e)
        {
            int? indexQuestion = await GetControllerIndex(e.senderName);
            int index;

            if (indexQuestion == null)
            {
                MessageBox.Show("Excel Control Index Error", "Some Weird Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                index = int.Parse(indexQuestion.ToString());
            }

            ExcelRule ruletoSave = new ExcelRule();
            ruletoSave.RuleName = excelControls[index].RuleName;
            ruletoSave.Cell1 = excelControls[index].Cell1Value;
            ruletoSave.Cell2 = excelControls[index].Cell2Value;

            ruletoSave.Operation = excelControls[index].Operation;

            if (ruletoSave.Operation == -1)
            {
                MessageBox.Show("Operation wasn't Selected", "Rule Operation Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ruletoSave.Criteria = excelControls[index].CriteriaValue;

            try
            {
                ruletoSave.Sheet1 = int.Parse(excelControls[index].Sheet1Value);
            }
            catch (Exception err)
            {
                MessageBox.Show("Please ensure Sheet 1 Value is a valid integer", "Sheet 1 Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                ruletoSave.Sheet2 = int.Parse(excelControls[index].Sheet2Value);
            }
            catch (Exception err)
            {
                MessageBox.Show("Please ensure Sheet 2 Value is a valid integer", "Sheet 2 Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string ruleToSaveString = JsonConvert.SerializeObject(ruletoSave, Formatting.Indented);
            byte[] ruleToSaveBytes = Encoding.ASCII.GetBytes(ruleToSaveString);

            SaveFileDialog fdSave = new SaveFileDialog();
            fdSave.Filter = "Excel Rule|*.exr";
            fdSave.DefaultExt = "exr";
            fdSave.Title = "Save Excel Rule";
            fdSave.OverwritePrompt = true;

            DialogResult dialogResult = fdSave.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                FileStream fs;

                if (File.Exists(fdSave.FileName))
                {
                    fs = new FileStream(fdSave.FileName, FileMode.Truncate);
                }
                else
                {
                    fs = new FileStream(fdSave.FileName, FileMode.CreateNew);
                }

                await fs.WriteAsync(ruleToSaveBytes, 0, ruleToSaveBytes.Length);

                fs.Close();
            }
        }

        private void ExcelCont_DeleteClickedHandler(object sender, ExcelConditionalControl.ExcelConditionalEventArgs e)
        {
            string objectName = e.senderName;

            Regex regex = new Regex(@"\d+$");

            Match match = regex.Match(objectName);

            if (match.Success)
            {
                int index = int.Parse(match.Value);
                RemovePanelItem(index);
                excelControls[index].PreviewClickedHandler -= ExcelConditionalControl_PreviewClickedHandler;
                excelControls[index].DeleteClickedHandler -= ExcelCont_DeleteClickedHandler;
                excelControls[index].Dispose();
                excelControls.RemoveAt(index);
                ReOrderControlList();
            }
        }

        private void RemovePanelItem(int index)
        {
            Action action = new Action(() =>
            {
                pnlExcelControl.Controls.Remove(excelControls[index]);
            });
            IAsyncResult resultIA = this.BeginInvoke(action);
            this.EndInvoke(resultIA);
        }

        private void ReOrderControlList()
        {
            int index = 0;

            foreach (ExcelConditionalControl excelCont in excelControls)
            {
                excelCont.Name = $"exCont{index}";
                index++;
            }
            runningNumber = index;

            if (runningNumber < 5)
            {
                this.AutoSize = true;
                this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
                this.AutoScroll = false;
            }

            if (index != 0)
            {

                Action action = new Action(() =>
            {
                pnlExcelControl.Height = excelControls[0].Height * (index);
            });
                IAsyncResult resultIA = this.BeginInvoke(action);
                this.EndInvoke(resultIA);
            }
        }

        private async Task<int?> GetControllerIndex(string ExcelcontrollerName)
        {
            Regex regex = new Regex(@"\d+$");

            Match match = regex.Match(ExcelcontrollerName);

            if (match.Success)
            {
                int index = int.Parse(match.Value);
                return index;
            }
            else
            {
                return null;
            }
        }

        private void frmExcelInteropt_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (appExcel != null)
            {
                appExcel.Quit();
            }
        }

        private void WorkbookTwoIsOne()
        {
            dummyWB = wbArray[0];
            wbArray[1] = wbArray[0];
            excelFileLoadControl2.LoadButtonEnable = false;
            excelFileLoadControl2.UnloadButtonEnable = true;
            excelFileLoadControl2.WorkbookFilePath = "Same as Workbook 1";
            excelFileLoadControl2.WorkbookInfo = "Same as Workbook 1";
        }

        private async void btnEvaluate_Click(object sender, EventArgs e)
        {
            if (wbArray[0] == null)
            {
                MessageBox.Show("Excel Workbook 1 not loaded", "Workbook Not Loaded Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (wbArray[1] == null)
            {
                DialogResult dialogResult = MessageBox.Show("Excel Workbook 2 not loaded. Use Workbook 1 for Comparision of Cells?", "Workbook Not Loaded Error",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

                if (dialogResult == DialogResult.OK)
                {
                    WorkbookTwoIsOne();
                }
                else
                {
                    return;
                }
            }

            if (excelControls.Count < 1)
            {
                MessageBox.Show("Nothing to Evaluate!", "Rules Control not Added", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (ExcelConditionalControl xlControl in excelControls)
            {
                try
                {
                    xlControl.SetColorFinalResult(SystemColors.ControlLight);
                    xlControl.SetColorResult1Agaist2(SystemColors.ControlLight);
                    xlControl.SetColorResult2Agaist1(SystemColors.ControlLight);
                    xlControl.FinalResult = "";
                    xlControl.Result1Agaist2 = "";
                    xlControl.Result2Agaist1 = "";

                    double operand1;
                    double operand2;
                    double xlCriteria = 0;

                    try
                    {
                        operand1 = wbArray[0]
                            .Worksheets[int.Parse(xlControl.Sheet1Value)]
                            .Range[xlControl.Cell1Value]
                            .Value2;
                    }
                    catch (Exception err)
                    {
                        xlControl.FinalResult = $"Error Cell 1: {err.Message}";
                        xlControl.SetColorFinalResult(Color.IndianRed);
                        continue;
                    }

                    try
                    {
                        operand2 = wbArray[1]
                           .Worksheets[int.Parse(xlControl.Sheet2Value)]
                           .Range[xlControl.Cell2Value]
                           .Value2;
                    }
                    catch (Exception err)
                    {
                        xlControl.FinalResult = $"Error Cell 2: {err.Message}";
                        xlControl.SetColorFinalResult(Color.IndianRed);
                        continue;
                    }

                    int xlOperator = xlControl.Operation;

                    if (xlOperator == -1)
                    {
                        xlControl.FinalResult = "Error Operator not set";
                        xlControl.SetColorFinalResult(Color.IndianRed);
                        continue;
                    }

                    if (xlOperator == 0)
                    {
                        try
                        {
                            xlCriteria = double.Parse(xlControl.CriteriaValue);
                        }
                        catch (Exception err)
                        {
                            xlControl.FinalResult = $"Error Criteria: {err.Message}";
                            xlControl.SetColorFinalResult(Color.IndianRed);
                            continue;
                        }
                    }

                    ExcelEvaluation xlEvaluate = new ExcelEvaluation
                    {
                        Operand1 = operand1,
                        Operand2 = operand2,
                        Operator = xlOperator,
                        Criteria = xlCriteria
                    };

                    await EvaluationTime(xlEvaluate, xlControl);
                }
                catch (Exception err)
                {
                    xlControl.FinalResult = $"Error: {err.Message}";
                    xlControl.SetColorFinalResult(Color.IndianRed);
                    continue;
                }
            }
        }

        private async Task EvaluationTime(ExcelEvaluation xlEvaluate, ExcelConditionalControl xlControl)
        {
            switch (xlEvaluate.Operator)
            {
                case 0:
                    double result1 = Math.Abs((xlEvaluate.Operand1 - xlEvaluate.Operand2) / xlEvaluate.Operand1) * 100;
                    double result2 = Math.Abs((xlEvaluate.Operand2 - xlEvaluate.Operand1) / xlEvaluate.Operand2) * 100;

                    xlControl.Result1Agaist2 = result1.ToString();
                    xlControl.Result2Agaist1 = result2.ToString();

                    Color colVal1 = result1 <= xlEvaluate.Criteria ? Color.LightGreen : Color.IndianRed;
                    Color colVal2 = result2 <= xlEvaluate.Criteria ? Color.LightGreen : Color.IndianRed;

                    xlControl.SetColorResult1Agaist2(colVal1);
                    xlControl.SetColorResult2Agaist1(colVal2);

                    if (result1 <= xlEvaluate.Criteria && result2 <= xlEvaluate.Criteria)
                    {
                        xlControl.FinalResult = "Passed from both sides";
                        xlControl.SetColorFinalResult(Color.LightGreen);
                    }
                    else if (result1 <= xlEvaluate.Criteria || result2 <= xlEvaluate.Criteria)
                    {
                        xlControl.SetColorFinalResult(Color.LightSeaGreen);
                        xlControl.FinalResult = "Partial Success";
                    }
                    else
                    {
                        xlControl.SetColorFinalResult(Color.IndianRed);
                        xlControl.FinalResult = "Criteria Failed";
                    }

                    break;

                case 1:
                    bool resultMoreThan = xlEvaluate.Operand1 > xlEvaluate.Operand2 ? true : false;

                    xlControl.Result1Agaist2 = xlEvaluate.Operand1.ToString();
                    xlControl.Result2Agaist1 = xlEvaluate.Operand2.ToString();

                    switch (resultMoreThan)
                    {
                        case true:
                            xlControl.SetColorFinalResult(Color.LightGreen);
                            xlControl.FinalResult = "Passed";
                            break;

                        case false:
                            xlControl.SetColorFinalResult(Color.IndianRed);
                            xlControl.FinalResult = "Failed";
                            break;
                    }
                    break;

                case 2:
                    bool resultLessThan = xlEvaluate.Operand1 < xlEvaluate.Operand2 ? true : false;

                    xlControl.Result1Agaist2 = xlEvaluate.Operand1.ToString();
                    xlControl.Result2Agaist1 = xlEvaluate.Operand2.ToString();

                    switch (resultLessThan)
                    {
                        case true:
                            xlControl.SetColorFinalResult(Color.LightGreen);
                            xlControl.FinalResult = "Passed";
                            break;

                        case false:
                            xlControl.SetColorFinalResult(Color.IndianRed);
                            xlControl.FinalResult = "Failed";
                            break;
                    }
                    break;

                default:
                    break;
            }
        }

        private void btnLoadBulkRule_Click(object sender, EventArgs e)
        {
            BulkExcelRule xlRules = new BulkExcelRule();

            OpenFileDialog fdOpen = new OpenFileDialog();
            fdOpen.Filter = "Bulk Excel Rule|*.bxr";
            fdOpen.DefaultExt = "bxr";
            fdOpen.Title = "Open Bulk Excel Rule";

            DialogResult dialogResult = fdOpen.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                FileStream fs = new FileStream(fdOpen.FileName, FileMode.Open);

                byte[] bulkRuleFromFileBytes = new byte[fs.Length];
                int fileSize = fs.Read(bulkRuleFromFileBytes, 0, bulkRuleFromFileBytes.Length);
                string bulkRuleFromFileString = Encoding.ASCII.GetString(bulkRuleFromFileBytes);

                try
                {
                    xlRules = JsonConvert.DeserializeObject<BulkExcelRule>(bulkRuleFromFileString);
                }
                catch (Exception err)
                {
                    MessageBox.Show("File Read Error. Please choose valid bxr file", "File Read Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                finally
                {
                    fs.Close();
                }

                if (excelControls.Count != 0)
                {
                    while (excelControls.Count != 0)
                    {
                        ExcelCont_DeleteClickedHandler(sender, new ExcelConditionalControl.ExcelConditionalEventArgs { senderName = $"ToDeleteFromBulk0" });
                    }
                }

                int indexRule = 0;

                foreach (ExcelRule xlRule in xlRules.xlRules)
                {
                    btnAddExcelCondControl_Click(sender, new EventArgs());

                    excelControls[indexRule].Cell1Value = xlRule.Cell1;
                    excelControls[indexRule].Cell2Value = xlRule.Cell2;
                    excelControls[indexRule].Sheet1Value = xlRule.Sheet1.ToString();
                    excelControls[indexRule].Sheet2Value = xlRule.Sheet2.ToString();
                    excelControls[indexRule].RuleName = xlRule.RuleName;
                    excelControls[indexRule].CriteriaValue = xlRule.Criteria;
                    excelControls[indexRule].Operation = xlRule.Operation;

                    indexRule++;
                }
            }
        }

        private async void btnSaveBulkRule_Click(object sender, EventArgs e)
        {
            BulkExcelRule xlRules = new BulkExcelRule();

            foreach (ExcelConditionalControl xlControl in excelControls)
            {
                ExcelRule ruletoSave = new ExcelRule();
                ruletoSave.RuleName = xlControl.RuleName;
                ruletoSave.Cell1 = xlControl.Cell1Value;
                ruletoSave.Cell2 = xlControl.Cell2Value;
                ruletoSave.Operation = xlControl.Operation;
                ruletoSave.Criteria = xlControl.CriteriaValue;

                try
                {
                    ruletoSave.Sheet1 = int.Parse(xlControl.Sheet1Value);
                }
                catch (Exception err)
                {
                    ruletoSave.Sheet1 = 0;
                }

                try
                {
                    ruletoSave.Sheet2 = int.Parse(xlControl.Sheet2Value);
                }
                catch (Exception err)
                {
                    ruletoSave.Sheet2 = 0;
                }

                xlRules.xlRules.Add(ruletoSave);
            }

            string bulkRuleToSaveString = JsonConvert.SerializeObject(xlRules, Formatting.Indented);
            byte[] bulkRuleToSaveBytes = Encoding.ASCII.GetBytes(bulkRuleToSaveString);

            SaveFileDialog fdSave = new SaveFileDialog();
            fdSave.Filter = "Bulk Excel Rule|*.bxr";
            fdSave.DefaultExt = "exr";
            fdSave.Title = "Save Bulk Excel Rule";
            fdSave.OverwritePrompt = true;

            DialogResult dialogResult = fdSave.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                FileStream fs;

                if (File.Exists(fdSave.FileName))
                {
                    fs = new FileStream(fdSave.FileName, FileMode.Truncate);
                }
                else
                {
                    fs = new FileStream(fdSave.FileName, FileMode.CreateNew);
                }

                await fs.WriteAsync(bulkRuleToSaveBytes, 0, bulkRuleToSaveBytes.Length);

                fs.Close();
            }
        }
    }
}
